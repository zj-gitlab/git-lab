# git + lab

A command line interface (CLI) for GitLab with
[offline issues](#experimental-offline-issues) (experimental).

> Distributed version control makes it possible to work efficiently in offline,
and poor connectivity environments, but modern planning and collaboration tools
are highly centralized and depend on a fast, stable internet connection to be
productive. `git-lab` bridges GitLab into these environments.

See [PURPOSE.md](PURPOSE.md) for full motivation.

## Usage

```bash
$ git-lab --help
CLI for GitLab

Usage:
  git-lab [flags]
  git-lab [command]

Available Commands:
  bundle      Bundles the local project cache into a gzipped tarball
  checkout    Checkout merge requests
  fetch       Fetches and caches the issues of the GitLab project
  help        Help about any command
  lint        Lints the GitLab CI configuration file
  list        Lists issues, projects, or pipelines
  search      Searches issues
  status      Prints GitLab commit status information
  view        View an issue, or job

Flags:
  -h, --help   help for git-lab

Use "git-lab [command] --help" for more information about a command.
```

## Getting Started

### Installing `git-lab`

With [go](https://golang.org/) installed, run:

```
GO111MODULE=on go install gitlab.com/jramsay/git-lab
```

### Configuration

`git-lab` is configured with a `~/.config/git-lab/gitlab.toml` TOML file, and
respects you `XDG_CONFIG_HOME` environment variable if you prefer a different
location.

```toml
[[server]]
host = "gitlab.com"
personal_access_token = "abc"

[[server]]
host = "gitlab.example.com"
personal_access_token = "xyz"
```

## Experimental: offline issues

> **ALPHA:** Offline issues is highly experimental and disabled by default.

Offline issues intends to make contributing to GitLab hosted projects easy in
a wide variety of environments where continuous high speed internet may not be
available, reliable, or affordable.

Supported commands are limited to and subject to change:

- `view issue [id]` from the local cache or live from the GitLab host
- `list issues` from the local cache or live from the GitLab host
- `fetch` issues from the GitLab host to the local cache
- `bundle` issues from the local cache to transfer between environments

## Want to contribute?

- See [CONTRIBUTING.md](CONTRIBUTING.md) for an overview of our processes
- See [DEVELOPMENT.md](DEVELOPMENT.md) for how to get started
