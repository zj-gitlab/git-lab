# Contributing to `git-lab`

First, thank you for taking the time to contribute! 🎉👍

When contributing to this project, please first discuss the change you wish to make via issue, email, or any other method with the owners of this project before making a change.

## How Can I Contribute?

### Reporting Bugs

Bugs are tracked as [GitLab issues](https://gitlab.com/jramsay/git-lab/issues?label_name[]=bug).

### Suggesting Enhancements

Enhancements are tracked as [GitLab issues](https://gitlab.com/jramsay/git-lab/issues?label_name[]=enhancement).

### Merge Requests

Before creating a merge request, please first discuss the change you wish to make by creating an issue.

Please read [DEVELOPMENT.md](DEVELOPMENT.md) for more details.
