package status

import (
	"testing"

	"gitlab.com/jramsay/git-lab/test"
)

func TestCommand_invalid(t *testing.T) {
	out, _, err := test.ExecuteCommand(Command(), "foobar")
	if err == nil {
		t.Errorf("No errors was defined. Output: %s", out)
	}
}
