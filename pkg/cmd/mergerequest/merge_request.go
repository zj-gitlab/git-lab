package mergerequest

import (
	"fmt"
	"io"
	"os"
	"path"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/pkg/cli"
)

// CheckoutOptions for checking out merge requests
type CheckoutOptions struct {
	MergeRequestID int
	Repository     *gitlab.Repository
}

// CheckoutCommand for checking out a merge request locally
func CheckoutCommand() *cobra.Command {
	opts := CheckoutOptions{}
	eg := `
# switches branch to the merge-ref for merge request 23 of the current project
git-lab checkout merge-request 23
`
	cmd := &cobra.Command{
		Use:          "merge-request [id]",
		Aliases:      []string{"mr"},
		Short:        "Checkout the merge request",
		Example:      eg,
		SilenceUsage: true,
		Args:         cli.ExactlyOneIID,
		RunE: func(cmd *cobra.Command, args []string) error {
			opts.MergeRequestID, _ = strconv.Atoi(args[0])
			opts.Repository = gitlab.NewRepository()

			return checkoutMergeRequest(cmd.OutOrStdout(), opts)
		},
	}

	return cmd
}

func checkoutMergeRequest(out io.Writer, opts CheckoutOptions) error {
	if !hasMergeRequestsRefSpec(opts.Repository) {
		fmt.Fprintln(out, "Ref doesn't exist, to fetch refs run `git config --add remote.origin.fetch \"+refs/merge-requests/*:refs/merge-requests/*\" && git fetch origin`")
		os.Exit(1)
	}

	mrRefDir := path.Join("refs", "merge-requests")
	refPath := path.Join(mrRefDir, strconv.Itoa(opts.MergeRequestID), "head")
	if !opts.Repository.RefExist(refPath) {
		fmt.Fprintln(out, "Missing ref, fetching...")
		if err := opts.Repository.Fetch(refPath); err != nil {
			return err
		}
	}

	return opts.Repository.Checkout(refPath)
}

func hasMergeRequestsRefSpec(repo *gitlab.Repository) bool {
	for _, line := range repo.ConfigGetAll("remote.origin.fetch") {
		if line == "+refs/merge-requests/*:refs/merge-requests/*" {
			return true
		}
	}

	return false
}
