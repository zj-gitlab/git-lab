package jobs

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
	"text/tabwriter"

	"github.com/lithammer/fuzzysearch/fuzzy"
	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/color"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/pkg/cli"
)

// Options for viewing and listing CI jobs
type Options struct {
	JobID      string
	Repository *gitlab.Repository
	Project    *gitlab.Project

	host      string
	projectID string
	remote    string
	branch    string
	commit    string
	format    string
}

// ViewCommand for viewing a CI job trace
func ViewCommand() *cobra.Command {
	opts := Options{}
	eg := `
# prints the job trace of job 239608423 from the current project
git-lab view job 237608426
`
	c := &cobra.Command{
		Use:          "job [id]",
		Short:        "Prints the CI job trace",
		Example:      eg,
		SilenceUsage: true,
		Args:         cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			opts.JobID = args[0]

			branch, err := getBranch(opts.branch)
			if err != nil {
				return err
			}

			commit, err := getCommit(opts.commit)
			if err != nil {
				return err
			}

			opts.Repository = gitlab.NewRepository(
				gitlab.WithRemote(opts.remote),
				gitlab.WithBranch(branch),
				gitlab.WithHEAD(commit),
			)

			// If project unspecified, use the current Git repository
			if opts.projectID == "" {
				project, err := gitlab.NewProjectForRepository(opts.Repository)
				if err != nil {
					return err
				}
				opts.Project = project
			}

			if opts.Project == nil {
				project, err := gitlab.NewProjectFromOptions(opts.host, opts.projectID)
				if err != nil {
					return err
				}
				opts.Project = project
			}

			return viewJob(cmd.OutOrStdout(), opts)
		},
	}

	cli.AddHostFlag(c, &opts.host)
	cli.AddProjectFlag(c, &opts.projectID)
	cli.AddFormatFlag(c, &opts.format)

	c.Flags().StringVarP(&opts.branch, "branch", "", "", "output format")
	c.Flags().StringVarP(&opts.commit, "commit", "", "", "output format")

	return c
}

// ListCommand for listing CI jobs
func ListCommand() *cobra.Command {
	opts := Options{}
	eg := `
# lists CI jobs for the current commit
git-lab list jobs 

# lists CI jobs from the project "gitlab-org/gitlab-ce" on the host "gitlab.com"
git-lab list jobs  --project gitlab-org/gitlab-ce --host gitlab.com
`
	cmd := &cobra.Command{
		Use:          "jobs",
		Aliases:      []string{"job"},
		Short:        "Lists the jobs of a project",
		Example:      eg,
		SilenceUsage: true,
		Args:         cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			branch, err := getBranch(opts.branch)
			if err != nil {
				return err
			}

			commit, err := getCommit(opts.commit)
			if err != nil {
				return err
			}

			opts.Repository = gitlab.NewRepository(
				gitlab.WithRemote(opts.remote),
				gitlab.WithBranch(branch),
				gitlab.WithHEAD(commit),
			)

			// If project unspecified, use the current Git repository
			if opts.projectID == "" {
				project, err := gitlab.NewProjectForRepository(opts.Repository)
				if err != nil {
					return err
				}
				opts.Project = project
			}

			if opts.Project == nil {
				project, err := gitlab.NewProjectFromOptions(opts.host, opts.projectID)
				if err != nil {
					return err
				}
				opts.Project = project
			}

			return listJobs(cmd.OutOrStdout(), opts)
		},
	}

	cmd.Flags().StringVarP(&opts.host, "host", "", "gitlab.com", "host")
	cmd.Flags().StringVarP(&opts.projectID, "project", "", "", "project identifier")
	cmd.Flags().StringVarP(&opts.branch, "branch", "", "", "output format")
	cmd.Flags().StringVarP(&opts.commit, "commit", "", "", "output format")
	cmd.Flags().StringVarP(&opts.format, "format", "o", "text", "output format")

	return cmd
}

func viewJob(out io.Writer, opts Options) error {
	if opts.Project == nil {
		return fmt.Errorf("No project")
	}

	if opts.Repository == nil {
		return fmt.Errorf("No repo")
	}

	jobID, err := parseJobIdentifier(opts.Project, opts.Repository, opts.JobID)
	if err != nil {
		return err
	}

	trace := gitlab.NewTraceReader(opts.Project, jobID)
	io.Copy(out, trace)

	return nil
}

func listJobs(out io.Writer, opts Options) error {
	var tWriter *tabwriter.Writer
	jobs, err := gitlab.FetchPipelineJobsByCommit(opts.Project, opts.Repository)
	if err != nil {
		return err
	}

	if opts.format == "text" {
		tWriter = tabwriter.NewWriter(out, 0, 0, 1, ' ', 0)
		defer tWriter.Flush()
	}

	for _, job := range jobs {
		switch opts.format {
		case "json":
			data, err := json.Marshal(job)
			if err != nil {
				return err
			}

			fmt.Fprintf(out, string(data))
		case "text":
			fmt.Fprintf(tWriter, "%d\t%s\t%s\t\n", job.ID, color.CommitStatus(job.Status, job.Status), job.Name)
		}
	}

	return nil
}

func parseJobIdentifier(project *gitlab.Project, repo *gitlab.Repository, id string) (int, error) {
	// optimistically parse as int
	jobID, err := strconv.Atoi(id)
	if err == nil {
		return jobID, nil
	}

	// fallback to treating it as a string name
	jobs, err := gitlab.FetchPipelineJobsByCommit(project, repo)
	if err != nil {
		return 0, fmt.Errorf("no jobs found for %s:%s", repo.Branch, repo.HEAD)
	}

	var matchingJobs []gitlab.Job
	for _, j := range jobs {
		if j.Name == id {
			matchingJobs = append(matchingJobs, j)
		} else if fuzzy.Match(id, j.Name) {
			matchingJobs = append(matchingJobs, j)
		}
	}

	if len(matchingJobs) == 0 {
		fmt.Printf("No jobs matching name: %s\n\n", id)
		os.Exit(1)
	} else if len(matchingJobs) > 1 {
		fmt.Println("Did you mean:")
		for _, j := range matchingJobs {
			fmt.Printf("  %s %s\n", color.CommitStatus(j.Status, gitlab.GlyphFromStatus(j.Status)), j.Name)
		}
		os.Exit(1)
	}

	return matchingJobs[0].ID, nil
}

// TODO: move to repo
func getBranch(ref string) (string, error) {
	if ref != "" {
		return ref, nil
	}

	branch, err := gitlab.RevParseAbbrev("HEAD")
	if err != nil {
		return "", gitlab.ErrDetachedHead
	}

	return branch, nil
}

// TODO: move to repo
func getCommit(sha string) (string, error) {
	if sha != "" {
		if gitlab.IsCommit(sha) == false {
			return "", fmt.Errorf("Not a valid commit: %s", sha)
		}

		return sha, nil
	}

	commit, err := gitlab.OidForRef("HEAD")
	if err != nil {
		return "", gitlab.ErrNoRepository
	}

	return commit, nil
}
