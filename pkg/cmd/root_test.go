package cmd

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/jramsay/git-lab/test"
)

func TestCommand_invalid(t *testing.T) {
	out, _, err := test.ExecuteCommand(Root())
	if err == nil {
		t.Errorf("No errors was defined. Output: %s", out)
	}

	expected := "Use one of available subcommands: bundle [file], checkout, fetch, help [command], lint [file], list, search, status, version, view"
	if d := cmp.Diff(expected, err.Error()); d != "" {
		t.Errorf("Unexpected output mismatch: %s", d)
	}
}
