package checkout

import (
	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/pkg/cmd/mergerequest"
)

// Command for checking out resources like merge requests
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "checkout",
		Short: "Checkout merge requests",
		Long:  ``,
	}

	cmd.AddCommand(
		mergerequest.CheckoutCommand(),
	)

	return cmd
}
