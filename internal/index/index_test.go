package index

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewIndex(t *testing.T) {
	tmp, err := ioutil.TempDir("", "")
	require.NoError(t, err)
	defer os.RemoveAll(tmp)

	os.Setenv("XDG_DATA_HOME", tmp)

	// When an index does not exist yet
	i, err := New("issues", nil)
	require.NoError(t, err)
	require.NotNil(t, i.Idx)
	require.NoError(t, i.Close())

	// When an index does exist
	i, err = New("issues", nil)
	require.NoError(t, err)
	require.NotNil(t, i.Idx)
	require.NoError(t, i.Close())
}

func TestPostWriteIndexesDocument(t *testing.T) {
	tmp, err := ioutil.TempDir("", "")
	require.NoError(t, err)
	defer os.RemoveAll(tmp)

	os.Setenv("XDG_DATA_HOME", tmp)

	i, err := New("issues", nil)
	require.NoError(t, err)
	defer i.Close()

	type Doc struct {
		Description string `json:"description"`
	}

	require.NoError(t, i.PostWrite(nil, 1, &Doc{"mepmep"}))

	// Now validate through a query
	result, err := i.Search("mepmep")
	require.NoError(t, err)

	require.Len(t, result.Hits, 1, "expected one document to be found")
}

func TestPostDeleteRemovesDocument(t *testing.T) {
	tmp, err := ioutil.TempDir("", "")
	require.NoError(t, err)
	defer os.RemoveAll(tmp)

	os.Setenv("XDG_DATA_HOME", tmp)

	i, err := New("issues", nil)
	require.NoError(t, err)
	defer i.Close()

	type Doc struct {
		Description string `json:"description"`
	}

	require.NoError(t, i.PostWrite(nil, 1, &Doc{"mepmep"}))

	// Now validate through a query
	result, err := i.Search("mepmep")
	require.NoError(t, err)
	require.Len(t, result.Hits, 1, "expected one document to be found")

	require.NoError(t, i.PostDelete(nil, 1))

	// Now validate through a query
	result, err = i.Search("mepmep")
	require.NoError(t, err)
	require.Len(t, result.Hits, 0, "expected no document to be found")
}
