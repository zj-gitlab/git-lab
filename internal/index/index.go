package index

import (
	"errors"
	"path/filepath"
	"strconv"

	"github.com/blevesearch/bleve"
	"gitlab.com/jramsay/git-lab/config"
)

var issuesIndexName = "issues.bleve"

type Index struct {
	Idx bleve.Index

	batch *bleve.Batch
}

// Options for opening and creating an Index
type Options struct {
	ReadOnly bool
}

func New(mapping string, opts *Options) (*Index, error) {
	dataDir := config.DataFilePath()
	readOnly := false
	if opts != nil {
		readOnly = opts.ReadOnly
	}

	switch mapping {
	case "issues":
		return getIssuesIndex(dataDir, readOnly)
	default:
		return nil, errors.New("Unimplemented index type")
	}

	return nil, nil
}

// PostWrite will write or update the
func (i *Index) PostWrite(namespace []byte, key int, value interface{}) error {
	return i.Idx.Index(strconv.Itoa(key), value)
}

// PostDelete will remove the key from the index
func (i *Index) PostDelete(namespace []byte, key int) error {
	return i.Idx.Delete(strconv.Itoa(key))
}

func (i *Index) PostBatchWrite(namespace []byte, key int, value interface{}) error {
	if i.batch == nil {
		i.batch = i.Idx.NewBatch()
	}

	if err := i.batch.Index(strconv.Itoa(key), value); err != nil {
		return err
	}

	if i.batch.Size() >= 100 {
		return i.BatchFlush()
	}

	return nil
}

// BatchFlush executes the current batch, and persists them to disk
func (i *Index) BatchFlush() error {
	if i.batch != nil {
		if err := i.Idx.Batch(i.batch); err != nil {
			return err
		}

		i.batch = i.Idx.NewBatch()
	}

	return nil
}

// Search leverages the built index to query for the query passed in
func (i *Index) Search(query string) (*bleve.SearchResult, error) {
	searchRequest := bleve.NewSearchRequest(bleve.NewQueryStringQuery(query))
	return i.Idx.Search(searchRequest)
}

func (i *Index) Close() error {
	if i.Idx != nil {
		if err := i.Idx.Close(); err != nil {
			return err
		}

		i.Idx = nil
	}

	return nil
}

// Only one per process can open a given index, the second call will block
var issuesIndex *Index

func getIssuesIndex(dataDir string, readOnly bool) (*Index, error) {
	if issuesIndex != nil {
		return issuesIndex, nil
	}

	path := filepath.Join(dataDir, issuesIndexName)

	config := map[string]interface{}{
		"read_only": readOnly,
	}

	i, err := bleve.OpenUsing(path, config)
	if err == bleve.ErrorIndexPathDoesNotExist {
		i, err = bleve.NewUsing(path, indexMapping(), "scorch", "scorch", nil)
		return &Index{Idx: i}, err
	}

	return &Index{Idx: i}, err
}
