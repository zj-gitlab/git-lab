package log

import (
	"bytes"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDebugf(t *testing.T) {
	var out bytes.Buffer
	logger.Out = &out

	Debugf("no")

	os.Setenv("GIT_LAB_DEBUG", "1")
	defer os.Setenv("GIT_LAB_DEBUG", "0")

	Debugf("yes")

	assert.Contains(t, out.String(), "msg=yes")
	assert.NotContains(t, out.String(), "msg=no")
}
