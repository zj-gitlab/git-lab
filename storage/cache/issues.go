package cache

import (
	"encoding/json"
	"time"

	"gitlab.com/jramsay/git-lab/gitlab"
)

var (
	epoch = time.Unix(0, 0)
)

const (
	issuesUpdatedAtKey = iota
)

// Implements IssueReader interface
func (cache *Cache) GetIssue(p *gitlab.Project, id int) (*gitlab.Issue, error) {
	issue := &gitlab.Issue{}

	err := cache.Get(issueProjectPrefix(p), id, issue)
	return issue, err
}

// Implements IssuesReader interface
func (cache *Cache) GetIssuesStream(p *gitlab.Project, opts *gitlab.IssuesStreamOpts, issueChan chan *gitlab.Issue) error {
	defer close(issueChan)

	cacheChan := make(chan []byte, 101)
	go cache.GetStream(issueProjectPrefix(p), cacheChan)

	for v := range cacheChan {
		issue := &gitlab.Issue{}
		err := json.Unmarshal(v, issue)
		if err != nil {
			return err
		}

		if opts == nil || opts.UpdatedAfter == nil || issue.UpdatedAt.After(*opts.UpdatedAfter) {
			issueChan <- issue
		}
	}

	return nil
}

func (cache *Cache) CountIssues(p *gitlab.Project, opts *gitlab.IssuesStreamOpts) int {
	// Fast unfiltered count using keys only
	if opts == nil {
		return cache.CountKeys(issueProjectPrefix(p))
	}

	// TODO: filtered count requires scanning all the records - not yet implemented
	return cache.CountKeys(issueProjectPrefix(p))
}

// Implements IssueWriter interface
func (cache *Cache) UpdateIssue(p *gitlab.Project, i *gitlab.Issue) error {
	return cache.Set(&Entry{Namespace: issueProjectPrefix(p), Key: i.IID, Value: i})
}

// Implements IssuesWriter interface
func (cache *Cache) UpdateIssuesStream(done chan struct{}, p *gitlab.Project, issueChan chan *gitlab.Issue) error {
	namespace := issueProjectPrefix(p)
	cacheChan := make(chan *Entry, 101)
	defer close(cacheChan)
	go cache.SetStream(done, cacheChan)

	for i := range issueChan {
		cacheChan <- &Entry{Namespace: namespace, Key: i.IID, Value: i}
	}

	return nil
}

func (cache *Cache) GetIssuesUpdatedAt(p *gitlab.Project) *time.Time {
	var timeStamp time.Time
	if err := cache.Get(metadataPrefix(p), issuesUpdatedAtKey, &timeStamp); err != nil {
		// If metadata is not set, fallback to scanning the issues
		if issue := cache.findLastUpdatedIssue(p); issue != nil {
			return issue.UpdatedAt
		}

		return &epoch
	}

	return &timeStamp
}

func (cache *Cache) SetIssuesUpdatedAt(p *gitlab.Project, u *time.Time) error {
	return cache.Set(&Entry{Namespace: metadataPrefix(p), Key: issuesUpdatedAtKey, Value: u})
}

// IndexIssues reindexs all issues for the given project
func (cache *Cache) IndexIssues(p *gitlab.Project, progress chan int) error {
	var e *Entry

	namespace := issueProjectPrefix(p)
	cacheChan := make(chan []byte, 101)
	go cache.GetStream(namespace, cacheChan)

	for v := range cacheChan {
		issue := &gitlab.Issue{}
		err := json.Unmarshal(v, issue)
		if err != nil {
			return err
		}

		e = &Entry{Namespace: namespace, Key: issue.IID, Value: issue}
		if err := cache.RunPostBatchWriteHooks(e); err != nil {
			return err
		}

		if progress != nil {
			progress <- 1
		}
	}

	if progress != nil {
		close(progress)
	}

	return cache.FlushPostBatchWriteHooks()
}

// Returns the string prefix to allow efficient filtering of the cache by key.
// Example: issues/gitlab.com/gitlab-org/gitaly
func issueProjectPrefix(p *gitlab.Project) []byte {
	return joinKeys([]byte("issues"), []byte(p.FullPath()))
}

func metadataPrefix(p *gitlab.Project) []byte {
	return joinKeys([]byte("meta"), []byte(p.FullPath()))
}

// Scan project issues to locate the most recently updated issue.
func (cache *Cache) findLastUpdatedIssue(p *gitlab.Project) *gitlab.Issue {
	readChan := make(chan *gitlab.Issue, 100)
	go cache.GetIssuesStream(p, nil, readChan)

	var lastUpdated *gitlab.Issue
	for i := range readChan {
		if lastUpdated == nil || i.UpdatedAt.After(*lastUpdated.UpdatedAt) {
			lastUpdated = i
		}
	}

	return lastUpdated
}
