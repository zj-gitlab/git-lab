package api

import (
	"time"

	"gitlab.com/jramsay/git-lab/gitlab"
)

func (api *GitLabAPI) GetCommit(p *gitlab.Project, id string) (*gitlab.Commit, error) {
	c, _, err := api.client.Commits.GetCommit(p.ID(), id, nil)
	if err != nil {
		return nil, err
	}

	now := time.Now()
	commit := &gitlab.Commit{*c, &now}

	return commit, nil
}
