# Purpose

Distributed version control makes it possible to work efficiently in offline,
and poor connectivity environments, but modern planning and collaboration tools
are highly centralized and depend on a fast, stable internet connection to be
productive. `git-lab` bridges GitLab into these environments.

As experienced with Git, offline access to every branch, and every commit in a
repository is tremendous for productivity. Providing fast, offline, searchable
and scriptable access to GitLab Issues and Epics should be similarly helpful.

## Background

Even as internet connectivity becomes increasingly like the air we breath,
everywhere and invisible, in many places and situations, by choice or
otherwise, not every person has continuous access to affordable, stable, fast
internet connectivity.

Even the most well serviced cities of the world suffer from internet outages,
but far more have fleeting access, be it because of geography, weather,
politics, travel, or even the wilful choice to disconnect to free oneself from
distraction and notifications.

It should be possible for anyone to participate and contribute using GitLab
without a permanent internet connection.

## Proposal

In order to be able to participate and contribute to a GitLab project without
consistent internet connection, I should be able to do the following offline:

- [x] search issues offline
- [x] view issues offline
- [ ] view issues discussions offline
- [ ] search epics offline
- [ ] view epics offline
- [ ] view epic discussions offline

Ideally, I should also be able to:

- [ ] edit issues offline
- [ ] edit epics offline
- [ ] edit discussions offline
- [ ] create issues offline
- [ ] create epics offline
- [ ] create discussions offline

More challengingly, I should be able:

- [ ] view code review comments offline
- [ ] create code review comments offline
