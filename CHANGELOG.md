# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added

- Version subcommand to print the git lab version https://gitlab.com/jramsay/git-lab/merge_requests/64
- Improved indexing feedback when fetching from a bundle https://gitlab.com/jramsay/git-lab/merge_requests/69

### Changed

- Use Bleve's `scorch` index format and store for better performance https://gitlab.com/jramsay/git-lab/merge_requests/62

### Fixed

- Use default cache options when no options are passed, not nil https://gitlab.com/jramsay/git-lab/merge_requests/65
- List issues seg fault caused by nil options https://gitlab.com/jramsay/git-lab/merge_requests/67
- Fetch hangs when trying to update the index https://gitlab.com/jramsay/git-lab/merge_requests/70

## [0.1.0] - 2019-07-05
### Added

- Add Job list https://gitlab.com/jramsay/git-lab/merge_requests/2
- Add GitLab CI Job Trace https://gitlab.com/jramsay/git-lab/merge_requests/3
- Allow configuration to be stored in XDG_CONFIG_DIR https://gitlab.com/jramsay/git-lab/merge_requests/7
- Add lint command to lint a .gitlab-ci.yml https://gitlab.com/jramsay/git-lab/merge_requests/13
- Allow a merge request to be checked out locally https://gitlab.com/jramsay/git-lab/merge_requests/16
- List projects subcommand https://gitlab.com/jramsay/git-lab/merge_requests/28
- Issues can be fetched into a local cache https://gitlab.com/jramsay/git-lab/merge_requests/35
- Support fast imports of issues through bundle import https://gitlab.com/jramsay/git-lab/merge_requests/54

### Fixed

- Fix pointer being used as Pipeline ID https://gitlab.com/jramsay/git-lab/merge_requests/10

[Unreleased]: https://gitlab.com/jramsay/git-lab/compare/v0.1.0...master
[0.1.0]: https://gitlab.com/jramsay/git-lab/compare/79298df2ec55e03d3a287beb8adee412331e...v0.1.0
