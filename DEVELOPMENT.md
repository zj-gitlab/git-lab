# Development guide for `git-lab`

## Getting started

1. Create a [GitLab account](https://gitlab.com)
1. Setup [GitLab access via SSH](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)
1. Create and [checkout a repo fork](#checkout-your-fork)
1. [Install requirements](#install-requirements)
1. [Set up your shell environment](#setup-environment)

### Checkout your fork

To check out this repository:

1. Create your own [fork of this repo]()
1. Clone it to your machine:

```
git clone git@gitlab.com:${YOUR_GITLAB_USERNAME}/git-lab.git
cd git-lab
git remote add upstream git@gitlab.com:jramsay/git-lab.git
git remote set-url --push upstream no_push
```

Adding the `upstream` remote makes it easy to regularly sync your fork.

### Install requirements

You must install these tools:

- [go 1.12](https://golang.org/doc/install): The language `git-lab` CLI is built
with.

### Setup environment

To build `git-lab` you will need to set the `GO111MODULE=on` environment
variable to force `go` to use
[`go modules`](https://github.com/golang/go/wiki/Modules#quick-start).

## Building `git-lab`

```
go build ./cmd/...
```

This builds the `git-lab` binary in your current directory.

```
go test ./...
```

This runs all the unit tests for `git-lab`.
